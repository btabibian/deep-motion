'''
  Created on Mar 1, 2015
   
  @author: btabibian
  '''
 
import argparse
from controller import GPPSF
import numpy as np
from scipy.stats import poisson
import pyfits
import os

parser = argparse.ArgumentParser(description='Produces sample PSF/trajectory pairs')
parser.add_argument('-l','--length', type=int, default= 4000,
                    help = 'Trajectory length.')
parser.add_argument('-n','--samples', type=int, default= 1000,
                    help = 'Number of sample trajectories')
parser.add_argument('-m','--mean', type=float, default= 40,
                    help = 'Mean(\mu) of delay distribution, distribution is a Poisson.')
parser.add_argument('-s','--shutter', type=int, default= 200,
                    help = 'Shutter length, length of the subset of the timeseries used to create PSF.')
parser.add_argument('-e','--expParam', type=tuple, default= [0.1,1.0/8.0], nargs = 2,
                    help = 'Gaussian Process parameters for trajectory sampling.')
parser.add_argument('-d','--dim', type=int, default= [1,1,1,1,1,1], nargs=6,
                    help = 'dimensions to filled with samples, 1 indicates the dimension to be filled \
                    and 0 fills the dimension with zeros.')
parser.add_argument('-o','--output', type=str, 
                    help = 'output directory')
parser.add_argument('-t','--test', action='store_true',
                    help = 'reopen last file and show description')
parser.add_argument('-v','--verbosity', type=float, default = 100, 
                    help = 'print update every v times.')
parser.add_argument('-b','--base', type = int, default = 0, help = 'base counter for output files')
parser.add_argument('-w','--window',type = int, default = 101, help = 'PSF image size')

if __name__ == '__main__':
  
  args = parser.parse_args()
  paths = np.zeros((6,args.length,args.samples))
  psfs = []
  shutter_vals = poisson.rvs(mu=args.mean,size = args.samples)
  shutter_length = args.shutter
  dim_index = np.array(args.dim, dtype=bool)
  for i in xrange(args.samples):
    samples_ = GPPSF.GPsamples(M = args.length,exphyp = args.expParam, D = dim_index.sum() )
    samples = np.zeros((args.length,6))
    samples[:,dim_index] = samples_
    index = np.min([shutter_vals[i],args.length-args.shutter]) - 1
    error = False
    try:
      psf = GPPSF.conevertSamples2psf(samples[index:(index+args.shutter)],args.window)
    except:
      ## Error in PSF
      print 'Error in constructing PSF, moving sample to error directory'
      psf = np.zeros((args.window))
      error = True
      
    #paths[:,:,i] = samples.T
    #psfs.append(psf)
    
    #write down
    hdu_img = pyfits.PrimaryHDU(psf)
    hdu_img.header['DESC'] = 'RAWImage'
    hdu_img.header['delay'] = shutter_vals[i]
    hdu_img.header['shutterL'] = args.shutter
    hdu_img.header['mu'] = args.mean
    col_gx = pyfits.Column(name='g_x', format='E', array=samples[:,0])
    col_gy = pyfits.Column(name='g_y', format='E', array=samples[:,1])
    col_gz = pyfits.Column(name='g_z', format='E', array=samples[:,2])
    col_ax = pyfits.Column(name='a_x', format='E', array=samples[:,3])
    col_ay = pyfits.Column(name='a_y', format='E', array=samples[:,4])
    col_az = pyfits.Column(name='a_z', format='E', array=samples[:,5])
    
    hdu_trajectory = pyfits.BinTableHDU.from_columns([col_gx,col_gy,col_gz,
                                               col_ax,col_ay,col_az],name = "trajectory")
    hdu_trajectory.header['exp'] = str(args.expParam)
    hdulist = pyfits.HDUList([hdu_img,hdu_trajectory])
    if error:
      hdulist.writeto(os.path.join(os.path.join(args.output,'error'),'%05d.fits' % (args.base+i))) 
    else:
      hdulist.writeto(os.path.join(args.output,'%05d.fits' % (args.base+i))) 
    if i % args.verbosity == 0:
      print 'sample %d out of total %d samples' % (args.base+i, args.samples)
    
  if args.test:
    fi_list = pyfits.HDUList.fromfile(os.path.join(args.output,'%05d.fits' % (args.base+i)))
    print 'PSF image shape: %s, #pixels >0: %.2f'% (str(fi_list[0].data.shape),(fi_list[0].data>0).sum())
    traj = np.array(map(list,fi_list[1].data))
    print 'trajectory data shape: %s' % str(traj.shape)
    print 'trajectory name: %s' % str(fi_list[1].columns.info())
    
    
    
    
