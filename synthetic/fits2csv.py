
import argparse
import pyfits
import numpy as np
import os
from scipy.ndimage import zoom
import re
parser = argparse.ArgumentParser(description='convert an input fits file to an output csv, input file name read from stdin and csv line printed on stdout.\n trajectory elements,shutter,image')
parser.add_argument('-i','--input', type = str,
                    help = 'input directory')
parser.add_argument('-o','--output', type = str,
                    help = 'output file')
parser.add_argument('-d','--downsample',type = float, default = -1,
                    help = 'downsampling factor,[0,1]')
parser.add_argument('-c','--clipe', action='store_true',help = 'clip trajectory to shutter length')

if __name__ == "__main__":  
  args = parser.parse_args()
  out = open(args.output,'w')
  files = sorted(os.listdir(args.input))

  for fi in files:
    if re.search("fit",fi) is None:
      continue
    fi_name = os.path.join(args.input,fi)
    fi_list = pyfits.HDUList.fromfile(fi_name)
    if args.downsample > 0:
      im_resize = zoom(fi_list[0].data,args.downsample,order = 1)
      im_resize = im_resize/np.sum(im_resize)
    else:
      im_resize = fi_list[0].data
    image = ','.join(map(str,im_resize.flatten().tolist()))
    traj_data = fi_list[1].data
    shutter = fi_list[0].header['shutterL']
    traj_data = fi_list[1].data[fi_list[0].header['delay']:fi_list[0].header['delay']+shutter]
    traj = map(list,traj_data)
    traj = ','.join(map(str,[item for sublist in traj for item in sublist]))
    out.write(','.join([str(len(traj.split(','))),traj,'1',str(shutter),str(fi_list[0].data.size),image])+'\n')
  out.close()
