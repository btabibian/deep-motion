'''
Created on 11.01.2011

@author: mhirsch
'''

import os
import serial
import time
#import scipy as sc
import numpy as np
import sys

import json
class Hexapod:
  
  def __init__(self):
    self.ser = serial.Serial(1, 57600)
    pass
  
  def init(self):
    self.ser.write('ERR?\n')
    if self.ser.readline() != '0\n':
      print "Initializing"
      self.ser.write('INI\n')
      self.ser.write('SPI R0 S0 T104\n')
    else:
      self.ser.write('SPI R0 S0 T104\n')
      self.ser.write('MOV X0 Y0 Z0 U0 V0 W0\n')
    #self.ser.write('VEL U14\n')
    self.ser.write('VEL 25') # default 4
    while True:
      self.ser.write('\005\n')
      if self.ser.readline() == '0\n':
        break
      time.sleep(0.1)
    print 'Initialized.'
    self.path = []
    self.start_time = time.clock()
  def clear_path(self):
    self.path = []
  def dist(self,x,y,z,x_0,y_0,z_0):
    return (x-x_0)**2+(y-y_0)**2+(z-z_0)**2
  def move(self, x_l=[0], y_l=[0], z_l=[0], u_l=[0], v_l=[0], w_l=[0]):
    #self.init()
    self.start_time = time.clock()
    self.path = []
    for i in xrange(len(x_l)):
      cmd = 'MOV! X%f Y%f Z%f U%f V%f W%f\n' % (x_l[i], y_l[i], z_l[i], u_l[i], v_l[i], w_l[i])
      print 'CMD %s' % cmd
      self.ser.write(cmd)
      while True:
        self.ser.write('\005\n')
        if self.ser.readline() == '0\n':
          print "Command Complete"
          if len(self.path)>0:
            print "Last Reading %s" % str(self.path[-1])
          break
        self.ser.write('\003\n')
        x = float(self.ser.readline()[3:-2])
        y = float(self.ser.readline()[3:-2])
        z = float(self.ser.readline()[3:-2])
        u = float(self.ser.readline()[3:-2])
        v = float(self.ser.readline()[3:-2])
        w = float(self.ser.readline()[3:-1])
        if self.dist(x,y,z,x_l[i],y_l[i],z_l[i])<1.0:
          break
        self.path.append(((time.clock()-self.start_time), x, y, z, u, v, w))
    return self.path
      
  def stop(self):
    self.ser.close()
    
  def resetClock(self):
    self.start_time = time.clock()

def init():
  hexapod = Hexapod()
  hexapod.init()
  hexapod.ser.close()
  return "Done"

def move(x=0, y=0, z=0, u=0, v=0, w=0, sleep = 0):
  hexapod = Hexapod()
  if sleep > 0:
    time.sleep(sleep)
  hexapod.move(x,y,z,u,v,w)
  hexapod.ser.close()
  return json.dumps(hexapod.path)

if __name__ == '__main__': 
  pod = Hexapod()
  print "move?"
  raw_input()
  pod.move(x=-11.0)
  raw_input()
  pod.move(x=0.0)
  raw_input()
  pod.move(x=11.0)
  print pod.path
  pod.stop()
  print ".Done"
