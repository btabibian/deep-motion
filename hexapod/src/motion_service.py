import motion
from flask import Flask,request
import json
app = Flask(__name__)

from rq import Queue
from rq.job import Job
from worker import conn

q = Queue('hexapod',connection=conn)

@app.route('/init')
def init():
  job = q.enqueue_call(
    func=motion.init ,result_ttl=5000)
  return job.get_id()

@app.route('/move')
def move():
  x_list = map(float,request.args.getlist('x'))
  y_list = map(float,request.args.getlist('y'))
  z_list = map(float,request.args.getlist('z'))
  u_list = map(float,request.args.getlist('u'))
  v_list = map(float,request.args.getlist('v'))
  w_list = map(float,request.args.getlist('w'))
  sleep = float(request.args.get('sleep'))
  print x_list
  job = q.enqueue_call(
    func=motion.move,args=(x_list,y_list,z_list,u_list,v_list,w_list,sleep) ,result_ttl=5000)
  return job.get_id()

@app.route("/results/<job_key>", methods=['GET'])
def get_results(job_key):

  job = Job.fetch(job_key, connection=conn)

  if job.is_finished:
    return str(job.result), 200
  else:
    return "Nay!", 202

if __name__ == "__main__":
  print conn
  app.run(host="0.0.0.0",debug = True)
