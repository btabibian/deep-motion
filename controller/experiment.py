
import urllib2
import time
import sys
sys.path.append("/usr/local/lib/python2.7/site-packages/")
import pyfits
import numpy as np

def get_loc_string(loc):
  return "&".join(map(lambda dat:"x=%f&y=%f&z=%f&u=%f&v=%f&w=%f"%(dat[0],dat[1],dat[2],dat[3],dat[4],dat[5]),loc))

def experiment(t,trajectory,init = None,hexpod = "10.42.2.20",raspberry = "10.42.2.22",port = 5000):
  loc_string = get_loc_string(trajectory)
  rasp_url = "http://%s:%d" %(raspberry,port)
  hex_url = "http://%s:%d" % (hexpod,port)
  if init is not None:
    init = get_loc_string(init)
    _ = urllib2.urlopen("http://10.42.2.20:5000/init").read()
    init_code = urllib2.urlopen(hex_url+"/move?%s&sleep=%f" % (init,0)).read()
  else:
    init_code = urllib2.urlopen(hex_url+"/init").read()
  while(True):
    print init_code
    result_init = urllib2.urlopen(hex_url+"/results/%s" % init_code).read()
    if result_init != "Nay!":
      break
    print 'waiting Hexapod'
    time.sleep(5)
  capture_code = urllib2.urlopen(rasp_url+"/capture").read()
  while(True):
    result_img_const = urllib2.urlopen(rasp_url+"/results/%s" % capture_code).read()
    if result_img_const != "Nay!":
      break
  capture_code = urllib2.urlopen(rasp_url+"/capture").read()
  #print loc_string
  print t
  move_code = urllib2.urlopen(hex_url+"/move?%s&sleep=%f" % (loc_string,t)).read()
  result_img = "Nay!"
  t_ = 0
  while result_img == "Nay!":
    result_img = urllib2.urlopen(rasp_url+"/results/%s" % capture_code).read()
    t_+=1
    print 'image not ready %d' % t_
    time.sleep(5)
    
  result_move = "Nay!"
  t_ = 0
  while result_move == "Nay!":
    result_move = urllib2.urlopen(hex_url+"/results/%s" % move_code).read()
    t_ +=1
    print 'hexapod not ready %d' % t_
    time.sleep(5)
  import base64
  data = base64.b64decode(result_img)
  fits = pyfits.HDUList.fromstring(data)
  data_const = base64.b64decode(result_img_const)
  fits_const = pyfits.HDUList.fromstring(data_const)
  import json
  grnd_truth = np.array(json.loads(result_move))
  dat = np.array(map(list,fits[1].data))
  return grnd_truth,dat,fits[0].data, fits_const[0].data