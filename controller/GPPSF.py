import numpy as np
from scipy.linalg import cholesky
from scipy.ndimage.measurements import center_of_mass

def dist(a,b = None):
  if b is None:
    mu = a.mean(axis=0)
    a = a - mu
    b = a
  else:
    mu = b.shape[0]/(a.shape[0]+b.shape[1])*b.mean(axis=0)+a.shape[0]/(a.shape[0]+b.shape[1])*a.mean(axis=0)
    a = a - mu
    b = b - mu
  return np.sum(a**2,axis = 1).reshape(a.shape[0],1)+np.sum(b**2,axis = 1).reshape(1,b.shape[0])-2*a.dot(b.T)
def covSEiso(hyp, x, z = None): 
  ell = np.exp(hyp[0])
  sf2 = np.exp(2*hyp[1])
  if z is None:
    K = dist(x.T/ell) 
  else:
    K = dist(x.T/ell,z.T/ell)
  K_ = sf2*np.exp(-K/2) + np.eye(K.shape[0])*1e-13
  return K_
def GPsamples(M = 1000, D = 3, exphyp = [0.1,1.0/8.0]):
  means = np.ones((M,1))*0.5
  t = np.linspace(0,1,M).reshape((M,1)).T
  kernel = lambda x,y:covSEiso(np.log(exphyp),x,y)
  covs = kernel(t,t)
  C = cholesky(covs)

  samples = np.zeros((M,D))
  for i in xrange(D):
    samples[:,i] = np.dot(C.T, np.random.normal(size=(C.shape[0],1))).flatten()+ means.flatten()
  
  return samples
def conevertSamples2psf(samples,length=101):
  max_length = 101
  s3d = [max_length,max_length,max_length]
  scaled = np.round((max_length-1)*samples+1).astype('int64')
  scaled = scaled[(np.min(scaled,axis=1)>0) & (np.max(scaled,axis=1)<=max_length),:]
  inds = np.ravel_multi_index([scaled[:,0],scaled[:,1],scaled[:,2]], dims=s3d, order='F')
  f = np.zeros(s3d)
  f[np.unravel_index(inds,s3d)] = 1
  f = f.sum(axis =2)
  shift = np.round((np.array(f.shape)+1)/2.0-center_of_mass(f)).astype('int64')
  f = np.roll(f,shift[0],axis =0)
  f = np.roll(f,shift[1],axis =1)
  return f/f.sum()