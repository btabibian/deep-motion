from matplotlib import cm
from DeepPSF.controller.utils import smooth
from scipy.interpolate import UnivariateSpline
import matplotlib.pyplot as plt
import numpy as np

def crop(data, point, size):
  return data[point[0]:(point[0]+size[0]),point[1]:(point[1]+size[1])]

def plot_(i,data,c_x=600,c_y=1200,w=400):
  plt.figure(figsize=(14,10))
  plt.subplot(2,3,1)
  plt.plot(data[i][1][:,[3,5,4]])
  plt.title('IMU data')
  
  plt.subplot(2,3,2)
  plt.plot(data[i][0][:,1:])
  plt.title('Hexapod data')
  
  plt.subplot(2,3,3)
  plt.plot(data[i][4])
  plt.title('Input trajectory')
  
  plt.subplot(2,3,4)
  smooth_path = np.zeros(data[i][0].shape)
  smooth_path[:,1] = smooth(data[i][0][:,1],window_len=70)
  smooth_path[:,2] = smooth(data[i][0][:,2],window_len=70)
  smooth_path[:,3] = smooth(data[i][0][:,3],window_len=70)
  vel = smooth_path[1:,1:4]-smooth_path[:-1,1:4]
  acc = (vel[1:,:]-vel[:-1,:])*4000 + data[i][1][:,[3,5,4]].mean(axis =0)
  plt.plot(acc)
  plt.title('Differetiated hexapod input')

  plt.subplot(2,3,5)
  #plt.imshow(data[i][2],interpolation = 'nearest')
  plt.title('captured PSF')
  plt.imshow(crop(data[i][2],(c_x,c_y),(w,w)),interpolation='nearest',cmap=cm.Greys_r,
             vmin = data[i][2].min(),vmax = data[i][2].max())
  
  plt.subplot(2,3,6)
  plt.title('stationary PSF')
  plt.imshow(crop(data[i][3],(c_x,c_y),(w,w)),interpolation = 'nearest',cmap=cm.Greys_r)

def plot_ind(i,data,c_x=600,c_y=1200,w=400):
  plt.figure(figsize=(14,14))
  plt.plot(data[i][1][:,[3,5,4]],linewidth = 3)
  plt.title('IMU data')
  
  plt.figure(figsize=(14,14))
  plt.plot(data[i][0][:,1:],linewidth = 3)
  plt.title('Hexapod data')
  
  plt.figure(figsize=(14,14))
  plt.plot(data[i][4],linewidth = 3)
  plt.title('Input trajectory')
  
  plt.figure(figsize=(14,14))
  smooth_path = np.zeros(data[i][0].shape)
  smooth_path[:,1] = smooth(data[i][0][:,1],window_len=20)
  smooth_path[:,2] = smooth(data[i][0][:,2],window_len=20)
  smooth_path[:,3] = smooth(data[i][0][:,3],window_len=20)
  vel = smooth_path[1:,1:4]-smooth_path[:-1,1:4]
  acc = (vel[1:,:]-vel[:-1,:])*40000 + data[i][1][:,[3,5,4]].mean(axis =0)
  plt.plot(acc,linewidth = 3)
  plt.title('Differetiated hexapod input')

  plt.figure(figsize=(14,14))
  #plt.imshow(data[i][2],interpolation = 'nearest')
  plt.title('captured PSF')
  plt.imshow(crop(data[i][2],(c_x,c_y),(w,w)),interpolation='nearest',cmap=cm.Greys_r,
             vmin = data[i][2].min(),vmax = data[i][2].max())
  
  plt.figure(figsize=(14,14))
  plt.title('stationary PSF')
  plt.imshow(crop(data[i][3],(c_x,c_y),(w,w)),interpolation = 'nearest',cmap=cm.Greys_r)