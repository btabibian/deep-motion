from adxl345 import SensorADXL345
from itg3200 import SensorITG3200
import picamera

import numpy as np
import io
import StringIO

import subprocess
from multiprocessing import Process, Queue, Semaphore
import os
import time

import pyfits

SHUTTER = 2000

def setup():
  #global camera,sensor_gy,sensor_ac
  camera = picamera.PiCamera()
  camera.resolution = (2592,1952)
  camera.raw_format = 'rgb'
  camera.start_preview()
  sensor_gy = SensorITG3200(1, 0x68) # update with your bus number and address
  sensor_ac = SensorADXL345(1, 0x53)
  sensor_gy.default_init()
  sensor_ac.default_init()
  time.sleep(1.7)
  return camera,sensor_gy,sensor_ac



def capture_acc(q,sensor_ac,sensor_gy):
  res = []
  while(True):
    #print q.qsize()
    ax, ay, az = sensor_ac.read_data()
    gx, gy, gz = sensor_gy.read_data()
    res.append((gx,gy,gz,ax,ay,az))
    time.sleep(0.001)
    if not q.empty():
      q.get()
      q.put(res)
      return

def capture_run():
  #global camera,sensor_gy,sensor_ac
  camera,sensor_gy,sensor_ac = setup()
  q = Queue()
  p = Process(target=capture_acc,args=(q,sensor_ac,sensor_gy))
  p.start()
  stream = io.BytesIO()
  camera.capture(stream,'raw')
  camera.shutter_speed = SHUTTER
  print 'done capture'
  q.put('done')
  p.join()
  res = np.array(q.get())
  #print camera.resolution, np.fromstring(stream.getvalue(), dtype=np.uint8).shape
  data_ = np.fromstring(stream.getvalue(), dtype=np.uint8)
  print data_.shape,camera.resolution 
  data = data_.reshape((camera.resolution[1],camera.resolution[0],3))
  hdu_img = pyfits.PrimaryHDU(data)
  hdu_img.header['DESC'] = 'RAWImage'
  print res.shape
  col_gx = pyfits.Column(name='g_x', format='int32', array=res[:,0])
  col_gy = pyfits.Column(name='g_y', format='int32', array=res[:,1])
  col_gz = pyfits.Column(name='g_z', format='int32', array=res[:,2])
  col_ax = pyfits.Column(name='a_x', format='int32', array=res[:,3])
  col_ay = pyfits.Column(name='a_y', format='int32', array=res[:,4])
  col_az = pyfits.Column(name='a_z', format='int32', array=res[:,5])
  hdu_imu = pyfits.BinTableHDU.from_columns([col_gx,col_gy,col_gz,col_ax,col_ay,col_az],name = "IMU")
  hdulist = pyfits.HDUList([hdu_img,hdu_imu])
  output = StringIO.StringIO()
  hdulist.writeto(output)
  import base64
  return base64.b64encode(output.getvalue())

