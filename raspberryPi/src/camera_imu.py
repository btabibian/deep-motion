import camera_imu_proc

from rq import Queue
from rq.job import Job
from worker import conn

from flask import Flask, Response
app = Flask(__name__)
q = Queue('raspberryPi',connection=conn)

@app.route('/setup')
def setup():
  job = q.enqueue_call(
    func=camera_imu_proc.setup,args=() ,result_ttl=5000)
  return job.get_id()

@app.route('/capture')
def capture():
  job = q.enqueue_call(
    func=camera_imu_proc.capture_run,args=() ,result_ttl=5000)
  return job.get_id()

@app.route("/results/<job_key>", methods=['GET'])
def get_results(job_key):
  
  job = Job.fetch(job_key, connection=conn)
  if job.is_finished:
    import base64
    return Response(job.result,mimetype='text/csv')
  else:
    return "Nay!", 202

if __name__ == '__main__':
    app.run(host='0.0.0.0',threaded = True)

  
