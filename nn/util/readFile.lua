
-- adapted from https://github.com/karpathy/char-rnn

require 'torch'
require 'csvigo'

local DataLoader = {}
DataLoader.__index = DataLoader



function DataLoader.create(data_dir, batch_size, split_fractions)
  local self = {}
  setmetatable(self, DataLoader)
  print(data_dir)
  local input_file = path.join(data_dir, 'input.csv')
  local tensor_file = path.join(data_dir, 'data.t7')
  
  if not path.exists(tensor_file) then
    print('one-time setup: preprocessing input text file ' .. input_file .. '...')
    DataLoader.text_to_tensor(input_file, tensor_file)
  end
  
  local data = torch.load(tensor_file)


  local len = data:size(1)
  print(len)

  if len % (batch_size) ~= 0 then
    print('cutting off end of data so that the batches/sequences divide evenly')
    data = data:sub(1, batch_size
                * math.floor(len / (batch_size)))
  end 
 
  -- look at fits2csv conversion
  self.batch_size = batch_size
  self.traj_length = data[1][1]
  self.shutterL = data[1][self.traj_length+3]
  self.image_length = data[1][self.traj_length+4]
  self.data = data

  local ydata = data:sub(1,-1,self.traj_length+5,-1)
  local xdata = data:sub(1,-1,1,self.traj_length+3)
  self.x_batches = xdata:split(batch_size, 1)  -- #rows = #batches
  self.y_batches = ydata:split(batch_size, 1)
  self.nbatches = #self.x_batches
  assert(#self.x_batches == #self.y_batches)
  self.ntrain = math.floor(self.nbatches * split_fractions[1])
  self.nval = math.floor(self.nbatches * split_fractions[2])
  self.ntest = self.nbatches - self.nval - self.ntrain
  self.split_sizes = {self.ntrain, self.nval, self.ntest}
  self.batch_ix = {0,0,0}
  
  print(string.format('data load done. Number of batches in train: %d, val: %d, test: %d', self.ntrain, self.nval, self    .ntest))
  collectgarbage()
  return self
end

function DataLoader:reset_batch_pointer(split_index, batch_index)
  batch_index = batch_index or 0
  self.batch_ix[split_index] = batch_index
end

function DataLoader:next_batch(split_index)
  -- split_index is integer: 1 = train, 2 = val, 3 = test
  self.batch_ix[split_index] = self.batch_ix[split_index] + 1
  if self.batch_ix[split_index] > self.split_sizes[split_index] then
    self.batch_ix[split_index] = 1 -- cycle around to beginning
  end
  -- pull out the correct next batch
  local ix = self.batch_ix[split_index]
  if split_index == 2 then ix = ix + self.ntrain end -- offset by train set size
  if split_index == 3 then ix = ix + self.ntrain + self.nval end -- offset by train + test
  ---print(self.y_batches[ix][{{1},{1,3}}])
  return self.x_batches[ix], self.y_batches[ix]
end

function DataLoader.text_to_tensor(in_textfile,  out_tensorfile)
    print('loading text file...')
    io.input(in_textfile)
    data = {}
    local j = 1
    local s = 0
    while true do
      t = io.read("*l")
      row = {}
      local i = 1
      if t == nil then
        break
      end
      nan_found = false
      for word in string.gmatch(t:gsub(',',' '), "%S+") do 
        row[i] = tonumber(word)
        s = s + row[i]
        if tostring(row[i]) == 'nan' then
          nan_found = true
        end
        i = i+1
      end
      if nan_found ~= true then
        data[j] = row
        j = j+1
      end
    end
    data = torch.Tensor(data)
    torch.save(out_tensorfile,data)
end
return DataLoader
