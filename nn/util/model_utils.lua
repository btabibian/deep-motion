
require 'torch'
local model_utils = {}

function model_utils.eval_split(split_index,max_batches,loader,model,opt)
  print('evaluating')
  local n = loader.split_sizes[split_index]
  if max_batches ~= nil then n = math.min(max_batches,n) end
  
  loader:reset_batch_pointer(split_index)
  
  local loss = 0
  samples = {}
  for i = 1,(loader.nbatches) do
    local x, y = loader:next_batch(split_index)
    samples[i] = {}
    samples[i].x = x[{{-opt.sample_val,-1},{}}]:clone():float()
    samples[i].y = y[{{-opt.sample_val,-1},{}}]:clone():float()
    if opt.gpuid >= 0 then
      x = x:cuda()
      y = y:cuda()
      
    end 
    local loss_x = criterion:forward(model:forward(x), y)
    samples[i].pred = model:forward(x):float()[{{-opt.sample_val,-1},{}}]:clone():float()
    loss = loss + loss_x
  end
  return loss,samples
end

return model_utils
