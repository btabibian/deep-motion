----------------------------------------------------------------------
-- example-linear-regression.lua
-- 
-- This script provides a very simple step-by-step example of
-- linear regression, using Torch7's neural network (nn) package,
-- and the optimization package (optim).
--

-- note: to run this script, simply do:
-- torch script.lua

-- to run the script, and get an interactive shell once it terminates:
-- torch -i script.lua

-- we first require the necessary packages.
-- note: optim is a 3rd-party package, and needs to be installed
-- separately. This can be easily done using Torch7's package manager:
-- torch-pkg install optim

require 'torch'
require 'optim'
local cjson = require "cjson"
--require 'cunn'
require 'nn'

local ReadFile = require 'nn.util.readFile'
local model_utils = require 'nn.util.model_utils'
----------------------------------------------------------------------
-- 1. Create the training data

-- In all regression problems, some training data needs to be 
-- provided. In a realistic scenarios, data comes from some database
-- or file system, and needs to be loaded from disk. In that 
-- tutorial, we create the data source as a Lua table.

-- In general, the data can be stored in arbitrary forms, and using
-- Lua's flexible table data structure is usually a good idea. 
-- Here we store the data as a Torch Tensor (2D Array), where each
-- row represents a training sample, and each column a variable. The
-- first column is the target variable, and the others are the
-- input variables.

-- The data are from an example in Schaum's Outline:
-- Dominick Salvator and Derrick Reagle
-- Shaum's Outline of Theory and Problems of Statistics and Economics
-- 2nd edition
-- McGraw-Hill
-- 2002

-- The data relate the amount of corn produced, given certain amounts
-- of fertilizer and insecticide. See p 157 of the text.

-- In this example, we want to be able to predict the amount of
-- corn produced, given the amount of fertilizer and intesticide used.
-- In other words: fertilizer & insecticide are our two input variables,
-- and corn is our target value.

--  {corn, fertilizer, insecticide}
cmd = torch.CmdLine()
cmd:text()

cmd:option('-data_dir','data/tinyshakespeare','data directory. Should contain the file input.txt with input data')
cmd:option('-batch_size',100,'number of sequences to train on in parallel')
cmd:option('-max_epochs',30,'number of full passes through the training data')
cmd:option('-train_frac',0.95,'fraction of data that goes into train set')
cmd:option('-val_frac',0.05,'fraction of data that goes into validation set')
cmd:option('-gpuid',0,'which gpu to use. -1 = use CPU')
cmd:option('-learning_rate',2e-3,'learning rate')
cmd:option('-decay_rate',0.95,'decay rate for rmsprop')
cmd:option('-eval_val_every',10,'evaluate validation every')
cmd:option('-checkpoint_dir','./checkpoints','checkpoints for loss')
cmd:option('-savefile','0','filename to save')
cmd:option('-sample_val','5','number of samples per batch to store in output')
cmd:text()

opt = cmd:parse(arg)

local test_frac = math.max(0, 1 - opt.train_frac - opt.val_frac)
local split_sizes = {opt.train_frac, opt.val_frac, test_frac}

local loader = ReadFile.create(opt.data_dir, opt.batch_size, split_sizes)


print(opt.gpuid)
if opt.gpuid >= 0 then
  print('using CUDA on GPU ' .. opt.gpuid .. '...')
  require 'cutorch'
  require 'cunn'
  cutorch.setDevice(opt.gpuid + 1) -- note +1 to make it 0 indexed! sigh lua
end

----------------------------------------------------------------------
-- 2. Define the model (predictor)

-- The model will have one layer (called a module), which takes the 
-- 2 inputs (fertilizer and insecticide) and produces the 1 output 
-- (corn).

-- Note that the Linear model specified below has 3 parameters:
--   1 for the weight assigned to fertilizer
--   1 for the weight assigned to insecticide
--   1 for the weight assigned to the bias term

-- In some other model specification schemes, one needs to augment the
-- training data to include a constant value of 1, but this isn't done
-- with the linear model.

-- The linear model must be held in a container. A sequential container
-- is appropriate since the outputs of each module become the inputs of 
-- the subsequent module in the model. In this case, there is only one
-- module. In more complex cases, multiple modules can be stacked using
-- the sequential container.

-- The modules are all defined in the neural network package, which is
-- named 'nn'.

model = nn.Sequential()                 -- define the container
print('traj_length' .. loader.traj_length)
print('image_lenght' .. loader.image_length)
ninputs = loader.traj_length+3; noutputs = loader.image_length
nhidden = 200
model:add(nn.Linear(ninputs, nhidden)):add(nn.Tanh()):add(nn.Linear(nhidden,nhidden)):add(nn.Tanh()):add(nn.Linear(nhidden,noutputs)) -- define the only module
local optim_state = {learningRate = opt.learning_rate, alpha = opt.decay_rate}

----------------------------------------------------------------------
-- 3. Define a loss function, to be minimized.

-- In that example, we minimize the Mean Square Error (MSE) between
-- the predictions of our linear model and the groundtruth available
-- in the dataset.

-- Torch provides many common criterions to train neural networks.

criterion = nn.MSECriterion()


if opt.gpuid >= 0 then
   for k,v in pairs({model,criterion}) do v:cuda() end
end

print('here')
----------------------------------------------------------------------
-- 4. Train the model

-- To minimize the loss defined above, using the linear model defined
-- in 'model', we follow a stochastic gradient descent procedure (SGD).

-- SGD is a good optimization algorithm when the amount of training data
-- is large, and estimating the gradient of the loss function over the 
-- entire training set is too costly.

-- Given an arbitrarily complex model, we can retrieve its trainable
-- parameters, and the gradients of our loss function wrt these 
-- parameters by doing so:

params, dl_dx = model:getParameters()
params:uniform(-0.08, 0.08)

-- In the following code, we define a closure, feval, which computes
-- the value of the loss function at a given point x, and the gradient of
-- that function with respect to x. x is the vector of trainable weights,
-- which, in this example, are all the weights of the linear matrix of
-- our mode, plus one bias.



feval = function(x)
   if x ~= params then
     params:copy(x)
   end
   -- set  to x_new, if differnt
   -- (in this simple example, x_new will typically always point to x,
   -- so the copy is really useless)
   local x_, y_ = loader:next_batch(1)
   -- print(y_:size())
   -- select a new training sample
   if opt.gpuid >= 0 then
    
     x_ = x_:float():cuda()
     y_ = y_:float():cuda()
   end
   -- reset gradients (gradients are always accumulated, to accomodate 
   -- batch methods)
   dl_dx:zero()
   -- evaluate the loss function and its derivative wrt x, for that sample
   local loss_x = criterion:forward(model:forward(x_), y_)
   model:backward(x_, criterion:backward(model.output, y_))
   --[[
   print('computed loss')
   print(dl_dx:size())
   print(loss_x)
   --]]
   -- return loss(x) and dloss/dx
   return loss_x, dl_dx
end

-- Given the function above, we can now easily train the model using SGD.
-- For that, we need to define four key parameters:
--   + a learning rate: the size of the step taken at each stochastic 
--     estimate of the gradient
--   + a weight decay, to regularize the solution (L2 regularization)
--   + a momentum term, to average steps over time
--   + a learning rate decay, to let the algorithm converge more precisely

local optim_state = {learningRate = opt.learning_rate, alpha = opt.decay_rate}

sgd_params = {
   learningRate = opt.learning_rate,
   learningRateDecay = 1e-4,
   weightDecay = 0,
   momentum = 0
}

-- We're now good to go... all we have left to do is run over the dataset
-- for a certain number of iterations, and perform a stochastic update 
-- at each iteration. The number of iterations is found empirically here,
-- but should typically be determinined using cross-correlation.

-- we cycle 1e4 times over our training data
for i = 1,opt.max_epochs do

   -- this variable is used to estimate the average loss
   current_loss = 0

   -- an epoch is a full loop over our training data
   for i = 1,(loader.nbatches) do

      -- optim contains several optimization algorithms. 
      -- All of these algorithms assume the same parameters:
      --   + a closure that computes the loss, and its gradient wrt to x, 
      --     given a point x
      --   + a point x
      --   + some parameters, which are algorithm-specific
      
      --  _,fs = optim.rmsprop(feval,params,optim_state)
         _,fs = optim.sgd(feval,params,sgd_params)
      -- Functions in optim all return two things:
      --   + the new x, found by the optimization method (here SGD)
      --   + the value of the loss functions at all points that were used by
      --     the algorithm. SGD only estimates the function once, so
      --     that list just contains one value. 
      
      current_loss = current_loss + fs[1]
   end
   if i % opt.eval_val_every == 0 or i == opt.max_epochs then
      local val_loss,samples = model_utils.eval_split(2,nil,loader,model,opt)
      local savefile = string.format('%s/lm_%s_epoch%.2f_%4f.t7', opt.checkpoint_dir,opt.savefile,i, val_loss)
      local saveSample = string.format('%s/lm_%s_sample%.2f_%4f.t7', opt.checkpoint_dir,opt.savefile,i, val_loss)
      local checkpoint = {}
      checkpoint.model = model
      checkpoint.train_loss = current_loss
      checkpoint.epoch = epoch
      checkpoint.val_loss = val_loss
      --cjson.encode(checkpoint) 
      torch.save(savefile,checkpoint)
      local saveSample = string.format('%s/lm_%s_sample%.2f_%4f.t7', opt.checkpoint_dir,opt.savefile,i, val_loss)
      torch.save(saveSample,samples)
      
   end      
   if i % 10 == 0 then collectgarbage() end
   -- report average error on epoch
   current_loss = current_loss
   if tostring(fs[1]) == tostring((-1)^0.5) then
     break
   end
   print('current loss = ' .. current_loss)

end


----------------------------------------------------------------------
-- 5. Test the trained model.

-- Now that the model is trained, one can test it by evaluating it
-- on new samples.

-- The text solves the model exactly using matrix techniques and determines
-- that 
--   corn = 31.98 + 0.65 * fertilizer + 1.11 * insecticides

-- We compare our approximate results with the text's results.

--text = {40.32, 42.92, 45.33, 48.85, 52.37, 57, 61.82, 69.78, 72.19, 79.42}
--[[
print('id  approx   text')
for i = 1,(#data)[1] do
   local myPrediction = model:forward(data[i][{{2,3}}])
   print(string.format("%2d  %6.2f %6.2f", i, myPrediction[1], text[i]))
end
--]]
